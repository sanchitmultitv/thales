import { Component, OnInit, ViewChild, ElementRef, OnDestroy, AfterViewInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';
declare var introJs: any;
declare var $: any;
import * as Clappr from 'clappr';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import { ChatService } from 'src/app/services/chat.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.scss']
})
export class LobbyComponent implements OnInit, OnDestroy, AfterViewInit {
  videoEnd = false;
  receptionEnd = false;
  showVideo = false;
  auditoriumLeft = false;
  auditoriumRight = false;
  exhibitionHall = false;
  registrationDesk = false;
  networkingLounge = false;
  resourceCentre = false;
  gameZone = false;
  intro: any;
  player: any;
  actives: any = [];
  liveMsg = false;
  showPoster = false;
  // timer:false;
  videoPlay2: any;

  // videoUrl;
  @ViewChild('recepVideo', { static: true }) recepVideo: ElementRef;

  constructor(private router:Router, private _fd: FetchDataService, private chat: ChatService,private toastr: ToastrService) { }
  //  videoPlayer = 'https://d3ep09c8x21fmh.cloudfront.net/servier_myanmar/lobby-animation.mp4';
   videoPlayer = '';
  ngOnInit(): void {
    // let timer: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();

    // if(timer >= '10:40:20'){
    //   alert(timer)
    // }
    if (localStorage.getItem('user_guide') === 'start') {
      this.getUserguide();
    }
    this.stepUpAnalytics('click_lobby');
    this.audiActive();
    this.chat.getconnect('toujeo-182');
    this.chat.getMessages().subscribe((data => {
      //  console.log('data',data);
      if (data == 'start_live') {
        this.liveMsg = true;
      }
      if (data == 'stop_live') {
        this.liveMsg = false;
      }

    }));

    let playVideo: any = document.getElementById("playVideo");
    window.onclick = (event) => {
      if (event.target == playVideo) {
        playVideo.style.display = "none";
        let pauseVideo: any = document.getElementById("videolob");
        pauseVideo.currentTime = 0;
        pauseVideo.pause();
      }
    }
  }

  endPlayscreen() {
    this.showPoster = true;
  }
  playScreenOnImg() {
    this.showPoster = false;
    let playVideo: any = document.getElementById("playVideo");
    playVideo.play();
  }
  ngAfterViewInit() {
    this.playAudio();
  }
  openModalVideo() {
    $('#playVideo').modal('show');
    let vid: any = document.getElementById("video");
    vid.play();
  }
  closeModalVideo() {
    let pauseVideo: any = document.getElementById("videolob");
    pauseVideo.currentTime = 0;
    pauseVideo.pause();
    $('#playVideo').modal('hide');
  }
  closePopup() {
    $('.yvideo').modal('hide');
    $('.pic1').modal('hide');
    $('.pic2').modal('hide');
    $('.pic3').modal('hide');
    $('.pic4').modal('hide');
    $('.pic5').modal('hide');
    $('.pic6').modal('hide');
    $('.pic7').modal('hide');
    $('.pic8').modal('hide');
    $('.note1').modal('hide');
    $('.note2').modal('hide');
    $('.note3').modal('hide');
  }
  openpopup() {
    if (localStorage.getItem('whatever')) {
      $('.NoFeedback').modal('show');
    } else {
      $('.feebackModal').modal('show');
    }
  }

  closePopuptwo() {
    $('.NoFeedback').modal('hide');
  }

  audiActive() {
    this._fd.activeAudi().subscribe(res => {
      this.actives = res.result;
    })
  }

  postPoints(points,module){
    let virtual: any = JSON.parse(localStorage.getItem('virtual'));
    this._fd.addLeaderboard(virtual.id,points,module).subscribe(res => {
      console.log('POINTS', res);
      // if(res.code === 1){
       // this.toastr.success(points + ' Points Added');
      // }else{

      // }
    });
  }

  @HostListener('window:resize', ['$event']) onResize(event) {
    // if (window.innerWidth <= 767) {
    //   this.player.resize({
    //     width: window.innerWidth / 6.69, height: window.innerHeight / 7.95
    //   });

    // } else {
    //   this.player.resize({
    //     width: window.innerWidth / 6.69, height: window.innerHeight / 7.95
    //   });
    // }
  }
  playAudio() {
    // localStorage.setItem('play', 'play');
    // let abc: any = document.getElementById('myAudio');
    // abc.play();
    // alert('after login audio')
  }
  playevent() {
    this.videoEnd = true;
  }
  playReception() {
    let timer: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    // alert(timer)
    // if (timer >= '18:59:59') {
      // alert('')
      this.receptionEnd = true;
      this.showVideo = true;
      let vid: any = document.getElementById("recepVideo");
      vid.play();
    // }else {
    //   $('.note3').modal('show');
    // }
    
    let pauseVideo: any = document.getElementById("videolob");
    pauseVideo.currentTime = 0;
    pauseVideo.pause();
  }
  receptionEndVideo() {
    this.router.navigate(['/auditorium/five']);

  }
  gotoKeyPortfolio() {
    this.registrationDesk = true;
    this.showVideo = true;
    let vid: any = document.getElementById("regDeskvideo");
    vid.play();
    let pauseVideo: any = document.getElementById("videolob");
    pauseVideo.currentTime = 0;
    pauseVideo.pause();
  }
  onEndKeyPortfolio() {
    this.router.navigate(['/keyportfolio']);

  }

  // gotoAuditoriumFront() {
  //   this.router.navigate(['/auditorium/front-desk']);
  // }

  playAuditoriumLeft() {
    // if (this.actives[3].status == true) {
      // let timer: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    // if (timer >= '8:59:59' && timer <= '17:59:59') {
      this.auditoriumLeft = true;
      this.showVideo = true;
      let vid: any = document.getElementById("audLeftvideo");
      vid.play();
    // }
    // else if (timer <= '8:58:59') {
      // $('.note1').modal('show');
    // }
    // else if (timer >= '18:01:01') {
      // $('.note2').modal('show');
    // }
    let pauseVideo: any = document.getElementById("videolob");
    pauseVideo.currentTime = 0;
    pauseVideo.pause();
  }
  // else {
  //   $('.audiModal').modal('show');
  // }

  // }
  gotoAuditoriumLeftOnVideoEnd() {
    this.router.navigate(['/networkingLounge']);
  }
  playAuditoriumRight() {
    // if (this.actives[2].status == true) {
    let timer: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    // if (timer >= '8:59:59' && timer <= '17:59:59') {
      this.auditoriumRight = true;
      this.showVideo = true;
      let vid: any = document.getElementById("audRightvideo");
      vid.play();
    // }
    // else if (timer <= '8:58:59') {
    //   $('.note1').modal('show');
    // }
    // else if (timer >= '18:01:01') {
    //   $('.note2').modal('show');
    // }
    let pauseVideo: any = document.getElementById("videolob");
    pauseVideo.currentTime = 0;
    pauseVideo.pause();
    // }
    // else {
    //   $('.audiModal').modal('show');
    // }
  }
  gotoAuditoriumRightOnVideoEnd() {
    this.router.navigate(['/resource']);
  }
  playExhibitionHall() {
    // let timer: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    //  alert(timer)
    // if (timer >= '8:59:59' && timer <= '17:59:59') {
      this.exhibitionHall = true;
      this.showVideo = true;
      let vid: any = document.getElementById("exhibitvideo");
      vid.play();
      let pauseVideo: any = document.getElementById("videolob");
      pauseVideo.currentTime = 0;
      pauseVideo.pause();
    // } 
    // else if (timer <= '8:58:59') {
    //   $('.note1').modal('show');
    // }
    // else if (timer >= '18:01:01') {
      // $('.note2').modal('show');
    // }
  }

  gotoExhibitionHallOnVideoEnd() {
    // this.router.navigate(['https://multitvtechsolutionpvtltd-390.my.webex.com/multitvtechsolutionpvtltd-390.my/j.php?MTID=m4c4cfb4001a7b7aeb3ce625324712a1b']);
    this.router.navigate(['/networkingLounge']);

    window.open("https://multitvtechsolutionpvtltd-390.my.webex.com/multitvtechsolutionpvtltd-390.my/j.php?MTID=m4c4cfb4001a7b7aeb3ce625324712a1b","_blank");

  }
  // playRegistrationDesk() {
  //   this.registrationDesk = true;
  //   this.showVideo = true;
  //   let vid: any = document.getElementById("regDeskvideo");
  //   vid.play();
  // }
  // gotoRegistrationDeskOnVideoEnd() {
  //   this.router.navigate(['/auditorium/front-desk']);
  // }
  playNetworkingLounge() {
    let timer: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    // alert(timer)
    // if (timer >= '8:59:59' && timer <= '17:59:59') {
      this.networkingLounge = true;
      this.showVideo = true;
      let vid: any = document.getElementById("netLoungevideo");
      vid.play();
      let pauseVideo: any = document.getElementById("videolob");
      pauseVideo.currentTime = 0;
      pauseVideo.pause();
    // } else if (timer <= '8:58:59') {
    //   $('.note1').modal('show');
    // }
    // else if (timer >= '18:01:01') {
      // $('.note2').modal('show');
    // }
  }
  playGamezone() {
   
      this.gameZone = true;
      this.showVideo = true;
      let vid: any = document.getElementById("gameZonevideo");
      vid.play();
      
  }
  playResourceCentre() {
    
      this.resourceCentre = true;
      this.showVideo = true;
      let vid: any = document.getElementById("resourceCentrevideo");
      vid.play();
  }

  gotoNetworkingLoungeOnVideoEnd() {
    /* this.router.navigate(['/exhibitionHall/life']); */
    this.router.navigate(['/networkingLounge']);

  }
  gotoCentreOnVideoEnd(){
    this.router.navigate(['/resource']);
  }
  gotogameZoneOnVideoEnd(){
    this.router.navigate(['/game-zone']);
  }
  lightbox_open() {
    //this.videoUrl = video;
    let lightBoxVideo: any = document.getElementById('VisaChipCardVideo');
    window.scrollTo(0, 0);
    document.getElementById('light').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
    lightBoxVideo.play();

  }
  lightbox_close() {
    let lightBoxVideo: any = document.getElementById('VisaChipCardVideo');
    document.getElementById('light').style.display = 'none';
    document.getElementById('fade').style.display = 'none';
    lightBoxVideo.pause();
  }
  stepUpAnalytics(action) {
    let virtual: any = JSON.parse(localStorage.getItem('virtual'));
    let yyyy: any = new Date().getFullYear();
    let dd: any = new Date().getDate();
    let mm: any = new Date().getMonth() + 1;
    let hrs: any = new Date().getHours();
    let mns: any = new Date().getMinutes();
    let secs: any = new Date().getSeconds();
    // let time: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    if (hrs < 10) {
      hrs = '0' + hrs;
    }
    if (mns < 10) {
      mns = '0' + mns;
    }
    if (secs < 10) {
      secs = '0' + secs;
    }
    const formData = new FormData();
    formData.append('event_id','182');
    formData.append('user_id', virtual.id);
    formData.append('name', virtual.name);
    formData.append('email', virtual.email);
    formData.append('company', 'multitv');
    formData.append('designation', 'others');
    formData.append('action', action);
    formData.append('created', yyyy + '-' + mm + '-' + dd + ' ' + hrs+':'+mns+':'+secs);
    this._fd.analyticsPost(formData).subscribe(res => {
      console.log('History', res);
    });
  }
  getUserguide() {
    this.intro = introJs().setOptions({
      hidePrev: true,
      hideNext: true,
      exitOnOverlayClick: false,
      exitOnEsc: false,
      steps: [
        {
          element: document.querySelector("#regDeskvideo"),
          intro: "<div style='text-align:center'>Click here to view Help Desk</div>"
        },
        {
          element: document.querySelectorAll("#networking_pulse")[0],
          intro: "<div style='text-align:center'>Click here to view Networking Lounge</div>"
        },
        {
          element: document.querySelectorAll("#audLeft_pulse")[0],
          intro: "<div style='text-align:center'>Click here to join auditorium</div>"
        },
        {
          element: document.querySelectorAll("#keyportfolio_pulse")[0],
          intro: "<div style='text-align:center'>Click here to join Keyportfolio</div>"
        },
        {
          element: document.querySelectorAll("#audRight_pulse")[0],
          intro: "<div style='text-align:center'>Click here to join Photo Contestant</div>"
        },
        {
          element: document.querySelectorAll("#resourcecentre_pulse")[0],
          intro: "<div style='text-align:center'>Click here to join Red Carpet</div>"
        },
        {
          element: document.querySelectorAll("#exhibition_pulse")[0],
          intro: "<div style='text-align:center'>Click here to join Exhibition</div>"
        },
      ]
    }).oncomplete(() => document.cookie = "intro-complete=true");

    let start = () => this.intro.start();
    start();
    // if (document.cookie.split(";").indexOf("intro-complete=true") < 0)
    //   window.setTimeout(start, 1000);
  }
  skipButton() {

    this.networkingLounge=false;
    this.router.navigateByUrl('/exhibitionHall/life');
  }
  skipButton2() {
    this.receptionEnd = false;
    this.router.navigateByUrl('/auditorium/five');
  }
  skipButton3() {
    this.exhibitionHall = false;
    // this.router.navigateByUrl('https://multitvtechsolutionpvtltd-390.my.webex.com/multitvtechsolutionpvtltd-390.my/j.php?MTID=m4c4cfb4001a7b7aeb3ce625324712a1b');
    this.router.navigate(['/networkingLounge']);
    window.open("https://multitvtechsolutionpvtltd-390.my.webex.com/multitvtechsolutionpvtltd-390.my/j.php?MTID=m4c4cfb4001a7b7aeb3ce625324712a1b","_blank");
  }
  skipButton4() {
    this.auditoriumRight = false;
    this.router.navigateByUrl('/capturePhoto');
  }
  skipButton5() {
    this.auditoriumLeft = false;
    this.router.navigateByUrl('/networkingLounge');
  }
  skipButton6() {
    this.auditoriumLeft = false;
    this.router.navigateByUrl('/game-zone');
  }
  skipButton7() {
    this.auditoriumLeft = false;
    this.router.navigateByUrl('/resource');
  }
  playGrowBySharingVideo() {
    // this.videosource = video;
    let playVideo: any = document.getElementById("videop");
    playVideo.play();
    $('#playVideos').modal('show');
  }
  closeModalVideos() {
    let pauseVideo: any = document.getElementById("videop");
    pauseVideo.currentTime = 0;
    pauseVideo.pause();
    $('#playVideos').modal('hide');
  }
  animationId;
  isAnimated=false;
  AnimationPlay(id){
    let video: any = document.getElementById('animateVideo');
    let sources = video.getElementsByTagName('source');
    this.animationId = id;
    let pauseVideo: any = document.getElementById("videolob");
    pauseVideo.currentTime = 0;
    pauseVideo.pause();
    
    if(id == 1){
      sources[0].src = 'https://d3ep09c8x21fmh.cloudfront.net/servier_myanmar/Networking lounge.mp4';
    }
    if(id == 2){
      sources[0].src = 'https://d3ep09c8x21fmh.cloudfront.net/servier_myanmar/Auditorium.mp4';
    }
    if(id == 3){
      sources[0].src = 'https://d3ep09c8x21fmh.cloudfront.net/servier_myanmar/Key Portfolio.mp4';
    }
    if(id == 4){
      sources[0].src = 'https://d3ep09c8x21fmh.cloudfront.net/servier_myanmar/Photocontest.mp4';
    }
    if(id == 5){
    // $('.pic5').modal('show');

      sources[0].src = 'https://d3ep09c8x21fmh.cloudfront.net/servier_myanmar/RedCarpet_a.mp4';
    }
    if(id == 6){
      sources[0].src = 'https://d3ep09c8x21fmh.cloudfront.net/servier_myanmar/Exhibition.mp4';
    }
    if(id == 7){
      sources[0].src = 'https://d3ep09c8x21fmh.cloudfront.net/servier_myanmar/Help Desk.mp4';
    }
    
    this.isAnimated = true;
    this.showVideo = true;
    video.load();
    video.play();
  }
  onEndAnimation(){
    if (this.animationId == 1){
      this.router.navigate(['/networkingLounge']);
    }
    if (this.animationId == 2){
      this.router.navigate(['/auditorium']);
    }
    if (this.animationId == 3){
      this.router.navigate(['/keyportfolio']);
    }
    if (this.animationId == 4){
      this.router.navigate(['/game-zone']);
    }
    if (this.animationId == 5){
      this.router.navigate(['/auditorium/right']);
    }
    if (this.animationId == 6){
      this.router.navigate(['/exhibitionHall/life']);
    }
    if (this.animationId == 7){
     this.router.navigate(['/appointments']);
    /*   $('.liveQuestionModal').modal('show') */
    }
  }
  ngOnDestroy() {
    if (localStorage.getItem('user_guide') === 'start') {
      let stop = () => this.intro.exit();
      stop();
    }
    localStorage.removeItem('user_guide');
  }
  closePopup1(){
    $('.feebackModal').modal('hide');
  }

}
