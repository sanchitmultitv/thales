import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-game-zone',
  templateUrl: './game-zone.component.html',
  styleUrls: ['./game-zone.component.scss']
})
export class GameZoneComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  
  closePopup() {
    $('#bubbleShoot').modal('hide');
    $('#bikeRacing').modal('hide');
  }
}
