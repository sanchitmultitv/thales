import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyContactsRoutingModule } from './my-contacts-routing.module';
import { MyContactsComponent } from './my-contacts.component';


@NgModule({
  declarations: [MyContactsComponent],
  imports: [
    CommonModule,
    MyContactsRoutingModule
  ]
})
export class MyContactsModule { }
