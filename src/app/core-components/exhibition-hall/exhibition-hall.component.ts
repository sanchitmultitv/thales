import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-exhibition-hall',
  templateUrl: './exhibition-hall.component.html',
  styleUrls: ['./exhibition-hall.component.scss']
})
export class ExhibitionHallComponent implements OnInit {
  videoEnd = false;
  constructor() { }

  ngOnInit(): void {
  }
  videoEnded() {
    this.videoEnd = true;
  }
}
