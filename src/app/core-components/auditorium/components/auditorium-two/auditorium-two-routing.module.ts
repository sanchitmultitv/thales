import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuditoriumTwoComponent } from './auditorium-two.component';


const routes: Routes = [  {path:'', component:AuditoriumTwoComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuditoriumTwoRoutingModule { }
