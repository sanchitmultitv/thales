import { Component, OnInit, OnDestroy} from '@angular/core';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import { GroupChatTwoComponent } from '../../../../layout/group-chat-two/group-chat-two.component';
declare var $:any;
@Component({
  selector: 'app-right-auditorium',
  templateUrl: './right-auditorium.component.html',
  styleUrls: ['./right-auditorium.component.scss'],
  providers:[GroupChatTwoComponent]
})
export class RightAuditoriumComponent implements OnInit, OnDestroy{
  videoEnd = false;
  interval;
  // videoPlayerTwo = 'https://dhzjgfv9krlhb.cloudfront.net/abr/smil:stream1.smil/playlist.m3u8';
  videoPlayerTwo;
  //videoPlayer = '../assets/video/networking_lounge_video.mp4';
  constructor(private gc:GroupChatTwoComponent, private _fd: FetchDataService) { }

  ngOnInit(): void {

    this.interval= setInterval(() => {
      this.getHeartbeat(); 
         }, 60000);

    // console.log(this.videoPlayerTwo,'this is 2');
    this._fd.activeAudi().subscribe((data=>{
      this.videoPlayerTwo = data.result[0].stream;
    }));

  }
  videoEnded() {
    this.videoEnd = true;
  }
  playAudioClap(){
    let playaudio : any= document.getElementById('myAudioClap');
    playaudio.play();
  }
  playAudioWhistle(){
    let playaudio : any= document.getElementById('myAudioWhistle');
    playaudio.play();
  }
  getHeartbeat(){
    // alert("ss")
    let data = JSON.parse(localStorage.getItem('virtual'));
    const formData = new FormData();
    formData.append('user_id', data.id );
    formData.append('event_id', '182');
    formData.append('audi', '205061');
    this._fd.heartbeat(formData).subscribe(res=>{
      console.log(res);
    })
  }

  openGroupChat(){
    $('.groupchatsModalTwo').modal('show')
    this.gc.loadData();
  
  }
  ngOnDestroy() {
    clearInterval(this.interval);
    // this.chatService.disconnect();
  }
}
