import { Component, OnInit, OnDestroy} from '@angular/core';
import { fadeAnimation } from '../../../../shared/animation/fade.animation';
import { FormGroup, FormControl, Validators, FormArray} from '@angular/forms';
import {FetchDataService} from '../../../../services/fetch-data.service'

import { EventEmitter } from 'events';
import { GroupChatComponent } from '../../../../layout/group-chat/group-chat.component';
declare var $: any;
@Component({
  selector: 'app-left-auditorium',
  templateUrl:'./left-auditorium.component.html',
  styleUrls: ['./left-auditorium.component.scss'],
  animations: [fadeAnimation],
  providers:[GroupChatComponent]
})
export class LeftAuditoriumComponent implements OnInit, OnDestroy {
  videoEnd = false;
  videoPlayer;
  interval;

  // videoPlayer = 'https://dhzjgfv9krlhb.cloudfront.net/abr/smil:stream3.smil/playlist.m3u8';
  // videoPlayer = '../assets/video/networking_lounge_video.mp4';
  constructor(private gc:GroupChatComponent, private _fd : FetchDataService) { }

  ngOnInit(): void {

    this.interval= setInterval(() => {
      this.getHeartbeat(); 
         }, 60000);

    console.log(this.videoPlayer,'this is 1');
    this._fd.activeAudi().subscribe((data=>{
      this.videoPlayer = data.result[1].stream;
    }));

  }
  videoEnded() {
    this.videoEnd = true;
  }
  playAudioClap(){
    let playaudio : any= document.getElementById('myAudioClap');
    playaudio.play();
  }
  playAudioWhistle(){
    let playaudio : any= document.getElementById('myAudioWhistle');
    playaudio.play();
  }
  getHeartbeat(){
    // alert("ss")
    let data = JSON.parse(localStorage.getItem('virtual'));
    const formData = new FormData();
    formData.append('user_id', data.id );
    formData.append('event_id', '182');
    formData.append('audi', '205059');
    this._fd.heartbeat(formData).subscribe(res=>{
      console.log(res);
    })
  }

  openGroupChat(){
    $('.groupchatsModal').modal('show')
    this.gc.loadData();
  }
  ngOnDestroy() {
    clearInterval(this.interval);
    // this.chatService.disconnect();
  }
}