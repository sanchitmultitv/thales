import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StageRoutingModule } from './stage-routing.module';
import { StageComponent } from './stage.component';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
@NgModule({
  declarations: [StageComponent],
  imports: [
    CommonModule,
    StageRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class StageModule { }
