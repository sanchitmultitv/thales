import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BriefcaseRoutingModule } from './briefcase-routing.module';
import { BriefcaseComponent } from './briefcase.component';
import { VgCoreModule } from 'videogular2/compiled/core';
import { VgControlsModule } from 'videogular2/compiled/controls';
import { VgOverlayPlayModule } from 'videogular2/compiled/overlay-play';
import { VgBufferingModule } from 'videogular2/compiled/buffering';
import { VgStreamingModule } from 'videogular2/compiled/streaming';


import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [BriefcaseComponent],
  imports: [
    CommonModule,
    BriefcaseRoutingModule,
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,
    VgStreamingModule,

   
   
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class BriefcaseModule {
 
 }
