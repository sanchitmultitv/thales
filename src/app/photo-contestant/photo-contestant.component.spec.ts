import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotoContestantComponent } from './photo-contestant.component';

describe('PhotoContestantComponent', () => {
  let component: PhotoContestantComponent;
  let fixture: ComponentFixture<PhotoContestantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhotoContestantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotoContestantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
