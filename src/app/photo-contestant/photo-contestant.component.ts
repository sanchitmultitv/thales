import { Component, OnInit } from '@angular/core';
import { FetchDataService } from '../services/fetch-data.service';

@Component({
  selector: 'app-photo-contestant',
  templateUrl: './photo-contestant.component.html',
  styleUrls: ['./photo-contestant.component.scss']
})
export class PhotoContestantComponent implements OnInit {
  quizlist: any =[];
  help:any =[];

  constructor(private _fd: FetchDataService,) { }

  ngOnInit(): void {
    this.getQuizlist();
    this.getCount();
  }

  

  vote(path,toid) {
    // let timer: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    // this.resetLINKS = this.router.url;
     let data = JSON.parse(localStorage.getItem('virtual'));

    const formData = new FormData();
     formData.append('event_id', '182');
     formData.append('user_id', data.id );
     formData.append('name', data.name );
     formData.append('comment', 'like' );
     formData.append('type', 'like_dislike' );
     formData.append('to_id', toid );
     formData.append('image', path );

    // let data = JSON.parse(localStorage.getItem('virtual'));
    // this.resetLINKS = this.router.url;

    // console.log("707007"+this.router.url);
    this._fd.postVote(formData).subscribe((res => {
      if (res.code === 1) {
        alert('Submitted Succesfully');
        this.getCount();
      }else{
        alert(res.result);
      }
    }))

  }

  getCount(){
    this._fd.getComments().subscribe((res: any) => {
      this.help = res.result;
      console.log(this.help);
      // alert(this.help)
      // this.help[0].
      // console.log('dddd', res)
    });
  }

  getQuizlist() {
    // let event_id = 182;
    this._fd.GetVoteData().subscribe((res: any) => {
      this.quizlist = res;
      console.log(this.quizlist);

      // console.log('dddd', res)
    });
  }
  
  closePopup(){
    alert('hello');
  }
}
