import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ChatService } from 'src/app/services/chat.service';
import { FetchDataService } from 'src/app/services/fetch-data.service';

@Component({
  selector: 'app-appoinments',
  templateUrl: './appoinments.component.html',
  styleUrls: ['./appoinments.component.scss']
})

export class AppoinmentsComponent implements OnInit {
  token = 'toujeo-182'; 
  deskMessages = [];
  message = new FormControl('');
  user_name;
  constructor(private chat: ChatService, private _fd: FetchDataService) { }

  ngOnInit(): void {
    this.gethelpdeskQuestions();
    this.chat.getconnect(this.token);
    this.chat.getSocketMessages(this.token).subscribe((res:any)=>{
      if(res.includes('question_reply')){
        this.gethelpdeskQuestions();
      }
      console.log('testing', res);
    });
  }
  rejectCall(id,attendee_id,name,email,time){
      // let data = JSON.parse(localStorage.getItem('virtual'));
      const formData = new FormData();
//       id:158887
// ec_name:divakar
// email:divakar.kumar@multitvsolution.com
// time:2020-12-01 10:10:10 
      formData.append('self_id', id );
      formData.append('attendee_id', attendee_id );
      formData.append('ec_name', name);
      formData.append('email', email);
      formData.append('time', time);
      this._fd.rejected(formData).subscribe(res=>{
        console.log(res);
        // this.getScheduleCallatttendee();
      })
  }
  
  gethelpdeskQuestions(){
    let data = JSON.parse(localStorage.getItem('virtual'));
    this.user_name = data.name;
    let event_id = data.event_id;
    this._fd.gethelpdeskanswers(data.id, event_id).subscribe((res=>{
      this.deskMessages = res.result;
    }))

  }

postHelpdesk(value){
  let data = JSON.parse(localStorage.getItem('virtual'));
  let event_id = data.event_id;
  if((value !== '')&&(value !== null)){
    this._fd.helpdesk(data.id,value, event_id).subscribe((res=>{
      if(res.code == 1){
        this.message.reset();
        let arr ={
          "question": value,
          "answer": ""
        };
        this.deskMessages.push(arr);
      }
    }));
  }
}
}
