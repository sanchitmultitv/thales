import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ChatService } from 'src/app/services/chat.service';
import { FetchDataService } from 'src/app/services/fetch-data.service';
declare var $: any;
@Component({
  selector: 'app-group-chat-two',
  templateUrl: './group-chat-two.component.html',
  styleUrls: ['./group-chat-two.component.scss']
})
export class GroupChatTwoComponent implements OnInit {
  textMessage = new FormControl('');
  newMessage: string[] = [];
  msgs: string;
  messageList: any=[];
  roomName= 'redcarpet';
  constructor(private chatService : ChatService, private _fd : FetchDataService) { }

  ngOnInit():void{
    // this.chatGroupTwo();
  }
  loadData() {
    this.chatGroupTwo();
    this.chatService.getconnect('toujeo-182');
    let data = JSON.parse(localStorage.getItem('virtual'));
    this.chatService.addUser(data.name, this.roomName);
    localStorage.setItem('username', data.name);
    this.chatService.receiveMessages(this.roomName).subscribe((msgs: any) => {
      if (msgs.roomId === 1) {
        this.messageList.push(msgs);
      }
      console.log('demo', this.messageList);
    });
  }
  chatGroupTwo() {
    this._fd.groupchating(this.roomName).subscribe(res => {
      console.log('groupChat', res);
      this.messageList = res.result;
    });
  }
  closePopup(){
    $('.groupchatsModalTwo').modal('hide');
  }
  postMessageTwo(value){
    let data = JSON.parse(localStorage.getItem('virtual'));
    let yyyy: any = new Date().getFullYear();
    let dd: any = new Date().getDate();
    let mm: any = new Date().getMonth() + 1;
    let time: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    let created = yyyy + '-' + mm + '-' + dd + ' ' + time;
  //  this.chatService.sendMessage(value, data.name, this.roomName);
  this._fd.postGroupchat(value,data.name,data.email, this.roomName, created,data.id).subscribe(res=>{
    this.chatGroupTwo();
    console.log(res);
  })



    this.textMessage.reset();
}
}
