import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FetchDataService } from 'src/app/services/fetch-data.service';
declare var $:any;
// import  * as html2canvas  from 'html2canvas';
// import html2canvas from 'html2canvas'; 
// import 'rxjs/Rx' ;
@Component({
  selector: 'app-active-audi',
  templateUrl: './active-audi.component.html',
  styleUrls: ['./active-audi.component.scss']
})
export class ActiveAudiComponent implements OnInit {
  name:any
  Certificateimage: any;
  main_image: any;
  downimage: any;
  
  constructor(private _fd: FetchDataService) { }

  ngOnInit(): void {
    let virtual: any = JSON.parse(localStorage.getItem('virtual'));
    this.name = virtual.name
    this.getCertify();
    
  }

  getCertify(){
    let virtual: any = JSON.parse(localStorage.getItem('virtual'));
    this._fd.getCertificate(virtual.id).subscribe((res => {
      this.Certificateimage = res;
      // alert(this.Certificateimage.result)
      this.main_image = "https://virtualapi.multitvsolution.com/"+this.Certificateimage.result;
      this.downimage = "https://thales-welcome-convention-2021.multitvsolution.com/assets/certificates/"+this.Certificateimage.result;
      console.log(this.Certificateimage)
    }))
    
  }
  closePopup(){
    $('.audiModal').modal('hide');
  } 
}
