import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-audio-screen',
  templateUrl: './audio-screen.component.html',
  styleUrls: ['./audio-screen.component.scss']
})
export class AudioScreenComponent implements OnInit, AfterViewInit {
  animateId;
  constructor(private router: Router, private ad: ActivatedRoute) { }

  ngOnInit(): void {
    // this.playAudio();
    this.ad.params.subscribe((params: Params) => {
      this.animateId = params.id;
      console.log(params.id);
    });
  }
  ngAfterViewInit(){
    this.playAnimation();
  }
  playAudio() {
    localStorage.setItem('play', 'play');
    let abc: any = document.getElementById('myAudio');
    abc.play();
    // alert('after login audio')
  }
  playAnimation(){
    let video: any = document.getElementById("animationVideo");
    let sources = video.getElementsByTagName('source');
    sources[0].src = 'https://d3ep09c8x21fmh.cloudfront.net/servier_myanmar/Exhibition.mp4';
    if(this.animateId == 1){
      sources[0].src = 'https://d3ep09c8x21fmh.cloudfront.net/servier_myanmar/Exhibition.mp4';
    }
    if(this.animateId == 2){
      sources[0].src = 'https://d3ep09c8x21fmh.cloudfront.net/servier_myanmar/Networking lounge.mp4';
    }
    if(this.animateId == 3){
      sources[0].src = 'https://d3ep09c8x21fmh.cloudfront.net/servier_myanmar/Exhibition.mp4';
    }
    if(this.animateId == 4){
      sources[0].src = 'https://d3ep09c8x21fmh.cloudfront.net/servier_myanmar/Exhibition.mp4';
    }
    if(this.animateId == 5){
      sources[0].src = 'https://d3ep09c8x21fmh.cloudfront.net/servier_myanmar/Exhibition.mp4';
    }
    video.load();
    video.play();
    
  }
  naviTolob() {
    this.router.navigate(['/lobby']);
  }
  OnEndVideo(){
    switch (this.animateId) {
      case 1:
        this.router.navigate(['/exhibitionHall']);
        break
      case 2:
        this.router.navigate(['/exhibitionHall']);
        break;
    
      default:
        break;
    }
  }
}
