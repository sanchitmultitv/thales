import { Component, OnInit } from '@angular/core';
import { FetchDataService } from '../../services/fetch-data.service';
declare var $: any;
@Component({
  selector: 'app-briefcase-docs',
  templateUrl: './briefcase-docs.component.html',
  styleUrls: ['./briefcase-docs.component.scss']
})
export class BriefcaseDocsComponent implements OnInit {
  documentsList = [];
  total: any;
  constructor(private _fd: FetchDataService) { }

  ngOnInit(): void {
    this.getBriefcaseList();
  }
  getBriefcaseList() {
    let virtual: any = JSON.parse(localStorage.getItem('virtual'));
    this._fd.getLeaderboard(virtual.id).subscribe((res: any) => {
      this.documentsList = res.result.result;
      this.total = res.result.total_points
      console.log(this.documentsList)
    });
  }
  closePopup() {
    $('.briefcaseModal').modal('hide');
  }
}
